libgnt extraction scripts
=========================

This repository contains the scripts and mappings used to extract the libgnt
history from the combined Pidgin repository.

Various unnecessary branches are leftover from a plain `hg convert`, which
causes a rather complicated [history full of useless merges](original.svg).

The subsequent `hg convert` use several splice maps to disconnect those
branches and then strips them out. See [the cleaned out history](clean.svg).

History can be graphed by copying the output (or piping to `xsel -b`) of:
```
hg log -T "{rev}|{p1rev}{ifeq(p2rev, -1, '', ' {p2rev}')}|{branches} {tags}\n" libgnt/
```
and pasting into the [Bit-Booster Offline Commit Graph Drawing
Tool](http://bit-booster.com/graph.html)

You can verify that the code is the same after branch deletion by running:

```
dir=libgnt
for tag in `hg tags -q -R $dir`; do
    (cd pidgin; hg checkout $tag)>/dev/null
    (cd $dir; hg checkout $tag)>/dev/null
    echo $tag \
        $(diff -uPNr -x .hg .hgtags pidgin/*/libgnt $dir | tee diff-$tag | wc -l)
done
```

You may also wish to double-check that the list of tags is equivalent to the
original Pidgin list of tags. Note that anything older than `v2.0.0` will not
exist because `libgnt` did not exist before then. Also, the tags will not
necessarily be on the same commits since commits that did not touch the
`libgnt` directory are gone.
