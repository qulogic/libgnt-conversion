#!/bin/bash

set -e

hg clone ssh://hg@bitbucket.org/pidgin/main pidgin
hg convert --filemap filemap pidgin libgnt-temp

hg convert --splicemap splicemap1 libgnt-temp libgnt-temp1

pushd libgnt-temp1
hg strip 1531 1518 1515 1313 1305 1301 1273 1252 1244 1200 1197 1083 1049 879 622 409

# Restore some stripped tags where they should now be:
hg update
cat >> .hgtags <<EOF
b3a71ff4780de169a3dbd2590119b8155f6c8372 v2.10.0
b3a71ff4780de169a3dbd2590119b8155f6c8372 v2.9.0
b3a71ff4780de169a3dbd2590119b8155f6c8372 v2.8.0
5416bbb5575a571d078c017442a44a44bf048a7a v2.7.11
5416bbb5575a571d078c017442a44a44bf048a7a v2.7.10
EOF
hg commit --amend -m 'update tags'
popd

# This branchmap deletes the cpw.malu.xmpp.google_refactor branch, as even
# though it's a bunch of merges, removing it loses some changes.
hg convert --splicemap splicemap2 --branchmap branchmap2 libgnt-temp1 libgnt-temp2

pushd libgnt-temp2
hg strip 1129 1082 1071 1047 1017 879

# Restore some stripped tags where they should now be:
hg update
cat >> .hgtags <<EOF
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.9
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.8
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.7
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.6
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.5
26f8714ecfdaf4711db67a92cfdb23f986997660 v2.7.4
1b09fcd0ada2cdd75950708c949b3907e9f42da3 v2.7.3
ef1b8cdb506f709cbc74ba736e835ac6fe2677fc v2.7.2
ef1b8cdb506f709cbc74ba736e835ac6fe2677fc v2.7.1
ef1b8cdb506f709cbc74ba736e835ac6fe2677fc v2.7.0
EOF
hg commit --amend -m 'update tags'
popd

hg convert --splicemap splicemap3 libgnt-temp2 libgnt-temp3

pushd libgnt-temp3
hg strip 1024 1023 994 941 872 866 861 855 833 832 809
popd

hg convert --splicemap splicemap4 libgnt-temp3 libgnt-temp4

pushd libgnt-temp4
hg strip 854 817 790 748 720 676 634 606 474 383
popd

hg convert --splicemap splicemap5 libgnt-temp4 libgnt-temp5

pushd libgnt-temp5
hg strip 839 790 742 710 563
popd

# This branchmap deletes the vv branch, as it's really a bug fix.
hg convert --splicemap splicemap6 --branchmap branchmap6 libgnt-temp5 libgnt-temp6

pushd libgnt-temp6
hg strip 790 788 769 737 736

# Restore some stripped tags where they should now be:
hg update
cat >> .hgtags <<EOF
1e1b88dfcbfbaa9eaab9cb94ff0d0acb087a227c v2.5.3
1e1b88dfcbfbaa9eaab9cb94ff0d0acb087a227c v2.5.3a
9ab969e07a04d0a2db45b2a9c78d8f9f9274d727 v2.5.1
9ab969e07a04d0a2db45b2a9c78d8f9f9274d727 v2.5.0
EOF
hg commit --amend -m 'update tags'
popd

# All this should do is clean up excess tags.
hg convert libgnt-temp6 libgnt

# Sort tag list a bit nicer.
pushd libgnt
hg update
../sorttags.py .hgtags > tags
mv tags .hgtags
hg commit --amend -m 'update tags'
popd
