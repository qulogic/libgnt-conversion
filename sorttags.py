#!/usr/bin/env python3

from distutils.version import LooseVersion
import fileinput


def version_key(tag):
    """
    Pick the last column (i.e., the tag) and version to LooseVersion.

    The underscore must be replaced for the one tag with weird style.
    """
    return LooseVersion(tag.split()[-1].replace('_', '.'))


tags = list(fileinput.input())
sorted_tags = sorted(tags, key=version_key)

print(*sorted_tags, sep='', end='')
